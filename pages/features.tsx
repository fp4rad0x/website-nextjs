// import { useRouter } from 'next/router'
import React from 'react'
import Box from '../src/components/Layout/Box'
import Flex from '../src/components/Layout/Flex'
import Text from '../src/components/Text'
import { useTranslation } from 'react-i18next'
import Image from 'next/image'
import UnderConstruction from '../public/img/stock/under-construction.png'

const NotFound: React.FC = (): JSX.Element => {
  // const router = useRouter()
  const { t } = useTranslation()
  const i18BasePath = 'pages.404'

  return (
    <Flex
      alignContent="center"
      alignItems="center"
      justifyContent="center"
      minHeight="30rem"
      gap="1rem"
    >
      <Box height="300px" width="300px">
        <Image src={UnderConstruction} />
      </Box>

      <Box textAlign="center">
        <Text bold fontSize="70px">
          Under construction
        </Text>
      </Box>
    </Flex>
  )
}

export default NotFound
