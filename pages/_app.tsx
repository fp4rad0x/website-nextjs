import type { AppProps } from 'next/app'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { ThemeProvider } from 'styled-components'
import '../locales/i18n'
import Footer from '../src/components/Footer'
import Container from '../src/components/Layout/Container'
import NavBar from '../src/components/NavBar'
import DeviceContext from '../src/context/devicesContexts'
import MaintainerContext from '../src/context/maintainersContext'
import SearchContext from '../src/context/searchContext'
import useNavBarItems from '../src/hooks/useNavBarItems'
import Device from '../src/models/Device'
import Maintainer from '../src/models/Maintainer'
import GlobalStyle from '../styles/globalStyles'
import darkTheme from '../styles/theme/dark'
import 'tippy.js/dist/tippy.css'
import 'tippy.js/themes/light.css'
import 'tippy.js/themes/material.css'
import HamburgerMenu from '../src/components/HamburgerMenu/Menu'
import Flex from '../src/components/Layout/Flex'
import useIsMobile from '../src/hooks/useIsMobile'
import { MOBILE_MENU_WIDTH } from '../src/constants/constants'

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  const [searchContext, setSearchContext] = useState('')
  const [deviceContext, setDeviceContext] = useState<Device[]>([])
  const [maintainerContext, setMaintainerContext] = useState<Maintainer[]>([])

  const [menuActive, setMenuActive] = useState(false)

  const NavBarItems = useNavBarItems()

  const router = useRouter()
  const isMobile = useIsMobile(MOBILE_MENU_WIDTH)

  const currentRoute = router.pathname

  return (
    <ThemeProvider theme={darkTheme}>
      <GlobalStyle />
      <Head>
        <title>AOSPK - The Kraken Project</title>
        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
        <meta
          name="description"
          content="AOSPK - The Kraken Project - The best Android experience"
        />
      </Head>

      {/* Toggled with css in src/components/HamburgerMenu/Menu/index.tsx */}
      <HamburgerMenu isActive={menuActive} setIsActive={setMenuActive} />

      <Flex direction="column" height="100%" justifyContent="space-between">
        <Container
          flex
          flexDirection="column"
          justifyContent="space-between"
          width="100%"
          padding="0"
        >
          <SearchContext.Provider value={{ searchContext, setSearchContext }}>
            <DeviceContext.Provider value={{ deviceContext, setDeviceContext }}>
              <MaintainerContext.Provider
                value={{ maintainerContext, setMaintainerContext }}
              >
                <NavBar
                  NavBarItems={NavBarItems}
                  menuActive={menuActive}
                  setMenuActive={setMenuActive}
                />

                {/* Necessary to not render the underlying components when the menu is active */}
                {isMobile && menuActive ? null : <Component {...pageProps} />}
              </MaintainerContext.Provider>
            </DeviceContext.Provider>
          </SearchContext.Provider>
        </Container>

        {/* Necessary to not render the underlying components when the menu is active */}
        {!menuActive && currentRoute !== '/404' ? <Footer /> : null}
      </Flex>
    </ThemeProvider>
  )
}

export default MyApp
