import React, { useContext, useState, useEffect, useMemo } from 'react'
import Container from '../../src/components/Layout/Container'
import Flex from '../../src/components/Layout/Flex'
import { v4 as uuidv4 } from 'uuid'
import DeviceCard from '../../src/components/DeviceCard'
import Input from '../../src/components/Input'
import Text from '../../src/components/Text'
import { FaSearch } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import SearchContext from '../../src/context/searchContext'
import DeviceContext from '../../src/context/devicesContexts'
import BrandFilter from '../../src/components/BrandFilter'
import MaintainerContext from '../../src/context/maintainersContext'
import fetchDevices from '../../src/services/fetchDevices'
import fetchMaintainers from '../../src/services/fetchMaintainers'
import { GetServerSideProps, GetServerSidePropsResult } from 'next'
import Device from '../../src/models/Device'
import Maintainer from '../../src/models/Maintainer'

interface DevicesProps {
  devices: Device[]
  maintainers: Maintainer[]
  children?: React.ReactNode
}

export const getServerSideProps: GetServerSideProps = async (
  _ctx
): Promise<
  GetServerSidePropsResult<{
    devices: Device[]
    maintainers: Maintainer[]
  }>
> => {
  const devices = await fetchDevices()
  const maintainers = await fetchMaintainers()

  const devicesData = devices.data
  const maintainersData = maintainers.data

  return {
    props: {
      devices: devicesData,
      maintainers: maintainersData,
    },
  }
}

const Devices: React.FC<DevicesProps> = ({
  devices,
  maintainers,
}): JSX.Element => {
  const { searchContext, setSearchContext } = useContext(SearchContext)
  const { deviceContext, setDeviceContext } = useContext(DeviceContext)
  const { maintainerContext, setMaintainerContext } =
    useContext(MaintainerContext)

  // initializing state with /deviceContext/ isnt working, had to set it on line
  // 78, why?
  const [devicesCopy, setDevicesCopy] = useState(deviceContext)

  // const [brands, setBrands] = useState(
  //   useMemo(
  //     () =>
  //       // Get only brand names and remove duplicates
  //       deviceContext
  //         .map((item) => item.brand)
  //         .filter((brand, index, array) => array.indexOf(brand) === index)
  //         .map((brand) => ({
  //           name: brand,
  //           active: false,
  //         })),
  //     [deviceContext]
  //   )
  // )

  const { t } = useTranslation()
  const i18BasePath = 'pages.devices'

  useEffect(() => {
    setTimeout(() => {
      setDeviceContext(devices)
      setDevicesCopy(devices)
      setMaintainerContext(maintainers)
      setSearchContext('')
    }, 300)
  }, [])

  // TODO - Add maintainers interpolation

  // fix - fix search
  useEffect(() => {
    // console.log(searchContext)
    // console.log(devices)
    // console.log(devicesCopy) // FIX - devices copy is empty wich breaks search

    const search = searchContext.toLowerCase()

    const queryResult = devicesCopy.filter((item) => {
      const name = item.name.toLowerCase()
      const brand = item.brand.toLowerCase()
      const codename = item.codename.toLowerCase()

      return [name, brand, codename].some((value) => value.includes(search))
    })

    setDeviceContext(queryResult)

    if (!searchContext) {
      setDeviceContext(devicesCopy)
    }
  }, [searchContext])

  return (
    <Container
      maxWidth="960px"
      width="100%"
      height="100%"
      flex
      flexDirection="column"
      gap="25px"
    >
      <Flex justifyContent="center" alignItems="center">
        <Input
          type="text"
          icon={<FaSearch />}
          placeholder={t(`${i18BasePath}.searchInputPlaceholder`)}
          maxLength={24}
          value={searchContext}
          onChange={(evt) => {
            const evtTarget = evt.target as HTMLInputElement
            setSearchContext(evtTarget.value)
          }}
        />
      </Flex>


      {/* <Flex
        justifyContent="center"
        alignContent="center"
        alignItems="center"
        gap="10px"
        flexWrap="wrap"
      >
        {brands?.map((brand) => (
          <BrandFilter
            key={brand.name}
            label={brand.name}
            active={brand.active}
            onClick={() => {
              setBrands((prev) =>
                prev.map((currentBrand) =>
                  currentBrand.name === brand.name
                    ? {
                        ...currentBrand,
                        active: currentBrand.active === true ? false : true,
                      }
                    : { ...currentBrand }
                )
              )

              const brandsToSearch = brands.filter(
                (brand) => brand.active === true
              )

              // setDeviceContext((prev) =>
              //   prev.filter(
              //     (item, idx) =>
              //       item.brand === brandsToSearch.map((brand) => brand)[idx]
              //   )
              // )

              const noBrandsToFilter = brands.every(
                (brand) => brand.active === false
              )

              if (noBrandsToFilter) {
                setDeviceContext(devices)
              }
            }}
          />
        ))}
      </Flex> */}

      <Flex justifyContent="center" gap="10px">
        <Flex gap="15px" flexWrap="wrap" justifyContent="center">
          {deviceContext.length ? (
            deviceContext.map((device) => (
              <DeviceCard
                key={uuidv4()}
                brand={device.brand}
                deviceName={device.name}
                deviceCodeName={device.codename}
                deprecated={device.deprecated}
              />
            ))
          ) : (
            <Text>{t(`${i18BasePath}.noItemFound`)}</Text>
          )}
        </Flex>
      </Flex>
    </Container>
  )
}

export default React.memo(Devices)
