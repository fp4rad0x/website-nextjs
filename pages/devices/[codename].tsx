/* eslint-disable sonarjs/cognitive-complexity */
import React, { useState } from 'react'
import Box from '../../src/components/Layout/Box'
import Container from '../../src/components/Layout/Container'
import Flex from '../../src/components/Layout/Flex'
import Text from '../../src/components/Text'
import BuildCard from '../../src/components/BuildCard'
import ArchiveButton from '../../src/components/ArchiveButton'
import {
  GetServerSideProps,
  GetServerSidePropsResult,
  GetStaticPaths,
  GetStaticPathsResult,
} from 'next'
import fetchDevices from '../../src/services/fetchDevices'
import DeviceDetailsProps from '../../src/contracts/DeviceDetailsProps'
import fetchDevice from '../../src/services/fetchDevice'
import ANDROID_VERSION from '../../src/models/Enums/ANDROID_VERSION'
import BUILD_TYPE from '../../src/models/Enums/BUILD_TYPE'
import { SingleBuild } from '../../src/models/DeviceBuild'
import Device from '../../src/models/Device'
import fetchDeviceSpecs from '../../src/services/fetchDeviceSpecs'
import { useTranslation } from 'react-i18next'
import { v4 } from 'uuid'
import endpoints from '../../src/services/endpoints'
import { useEffect } from 'react'
import useIsMobile from '../../src/hooks/useIsMobile'
import NoBuildsAvailableYet from '../../src/components/NoBuildsAvailableYet'
import Select from '../../src/components/Select'

// export const getStaticPaths: GetStaticPaths = async (): Promise<
//   GetStaticPathsResult<{ codename: string }>
// > => {
//   const devices = await fetchDevices()

//   const paths = devices.data.map((device) => {
//     return {
//       params: {
//         codename: device.codename,
//       },
//     }
//   })

//   return {
//     paths,
//     fallback: false,
//   }
// }

export const getServerSideProps: GetServerSideProps = async (
  ctx
): Promise<GetServerSidePropsResult<any>> => {
  // Fix typing
  const codename = ctx?.params?.codename

  const devices = await fetchDevices()
  const device = devices.data.find(
    (device: Device) => device.codename === codename
  )

  let elevenVanilla
  let elevenGapps

  if (
    device?.supported_versions?.includes(ANDROID_VERSION.ELEVEN) &&
    device.supported_types?.includes(BUILD_TYPE.VANILLA)
  ) {
    // Fetch vanilla eleven
    elevenVanilla = await fetchDevice(
      ANDROID_VERSION.ELEVEN,
      BUILD_TYPE.VANILLA,
      codename as string
    )
  }

  if (
    device?.supported_versions?.includes(ANDROID_VERSION.ELEVEN) &&
    device.supported_types?.includes(BUILD_TYPE.GAPPS)
  ) {
    // Fetch gapps eleven
    elevenGapps = await fetchDevice(
      ANDROID_VERSION.ELEVEN,
      BUILD_TYPE.GAPPS,
      codename as string
    )
  }

  let twelveVanilla
  let twelveGapps

  if (
    device?.supported_versions?.includes(ANDROID_VERSION.TWELVE) &&
    device.supported_types?.includes(BUILD_TYPE.VANILLA)
  ) {
    // Fetch vanilla twelve
    twelveVanilla = await fetchDevice(
      ANDROID_VERSION.TWELVE,
      BUILD_TYPE.VANILLA,
      codename as string
    )
  }

  if (
    device?.supported_versions?.includes(ANDROID_VERSION.TWELVE) &&
    device.supported_types?.includes(BUILD_TYPE.GAPPS)
  ) {
    // Fetch gapps twelve
    twelveGapps = await fetchDevice(
      ANDROID_VERSION.TWELVE,
      BUILD_TYPE.GAPPS,
      codename as string
    )
  }

  const deviceSpecs = await fetchDeviceSpecs(codename as string)

  return {
    props: {
      eleven: {
        vanilla: elevenVanilla?.data ?? [],
        gapps: elevenGapps?.data ?? [],
      },
      twelve: {
        vanilla: twelveVanilla?.data ?? [],
        gapps: twelveGapps?.data ?? [],
      },
      deviceDetails: device,
      deviceSpecs: deviceSpecs.data ?? {},
    },
  }
}

const DeviceDetails: React.FC<DeviceDetailsProps> = ({
  eleven,
  twelve,
  deviceDetails,
  deviceSpecs,
}): JSX.Element => {
  const [releaseDate, setReleaseDate] = useState('')
  const [builds, setBuilds] = useState<any>({
    eleven: {
      vanilla: [],
      gapps: [],
    },
    twelve: {
      vanilla: [],
      gapps: [],
    },
  })
  
  const [selectedAndroidVersion, setSelectedAndroidVersion] =
  useState<ANDROID_VERSION>(builds?.twelve.length ? ANDROID_VERSION.TWELVE : ANDROID_VERSION.ELEVEN)

  interface IBuilds {
    eleven: SingleBuild[] | []
    twelve: SingleBuild[] | []
  }

  const getBuildsToMap = (): IBuilds => {
    const result: IBuilds = {
      eleven: [],
      twelve: [],
    }

    // Check vanilla eleven
    if (eleven?.vanilla?.response?.length) {
      result.eleven.push(
        eleven?.vanilla?.response[eleven.vanilla.response.length - 1] as never
      )
    }

    // Check gapps eleven
    if (eleven?.gapps?.response?.length) {
      result.eleven.push(
        eleven?.gapps?.response[eleven.gapps.response.length - 1] as never
      )
    }

    // Check vanilla twelve
    if (twelve?.vanilla?.response?.length) {
      result.twelve.push(
        twelve?.vanilla?.response[twelve.vanilla.response.length - 1] as never
      )
    }

    // Check gapps twelve
    if (twelve?.gapps?.response?.length) {
      result.twelve.push(
        twelve?.gapps?.response[twelve.gapps.response.length - 1] as never
      )
    }

    return result
  }

  const { t } = useTranslation()
  const isMobile = useIsMobile()
  const i18BasePath = 'pages.device'

  const deviceImage = endpoints.image(deviceDetails?.codename as string)

  useEffect(() => {
    const builds = getBuildsToMap()
    setBuilds(builds)
  }, [])

  useEffect(() => {
    if (!deviceSpecs) {
      return
    }

    // Fix for 'window is undefined'
    // https://stackoverflow.com/questions/55151041/window-is-not-defined-in-next-js-react-app
    const usersLocale = window.navigator.languages[0]
    const releaseDate = new Date(String(deviceSpecs?.release))
    const formattedReleaseDate = releaseDate.toLocaleDateString(usersLocale, {
      year: 'numeric',
      month: 'long',
    })

    setReleaseDate(formattedReleaseDate)
  }, [])

  const convertSizeToMb = (size: number): string =>
    (size / 1024 ** 2).toFixed(2).toString()
    
    console.log(builds)

  return (
    <Container
      id="DEVICE-deviceSpecsContainer"
      maxWidth="960px"
      flex
      gap="1rem"
      width="100%"
      justifyContent="space-between"
      padding="1rem"
    >
      <Flex direction="column" gap="1rem" width="100%" padding="1rem">
        <Flex>
          <Text fontSize="xx-large" fontWeight={600}>
            {`${deviceDetails?.name} (${deviceDetails?.codename})`}
          </Text>
        </Flex>

        <Flex width="100%" direction="column" gap="1rem">
          <Box>
            <Text bold fontSize="x-large">
              {t(`${i18BasePath}.downloads`)}
            </Text>
          </Box>

          <Flex
            gap="1rem"
            justifyContent="space-between"
            alignContent="center"
            alignItems="center"
          >
            <Box>
              <Text>{`${t(`${i18BasePath}.android-version`)}:`}</Text>
            </Box>
            
            <Box width="60%">
              {/* // TODO - style select component */}
              <Select
                style={{ width: '100%', padding: '0.5rem', color: 'white' }}
                onChange={(evt: React.ChangeEvent<HTMLSelectElement>) => {
                  const value = evt.target.value
                  setSelectedAndroidVersion(value as ANDROID_VERSION)
                }}
              >
                <option style={{color: 'white'}} label="11 (eleven)" value={ANDROID_VERSION.ELEVEN} />
                <option style={{color: 'white'}} label="12 (twelve)" value={ANDROID_VERSION.TWELVE} />
              </Select>
            </Box>
          </Flex>
        </Flex>

        <Flex direction="column" gap="1rem">
          {builds?.[selectedAndroidVersion]?.length ? (
            builds?.[selectedAndroidVersion].map((build: SingleBuild) => (
              <BuildCard
                key={build.id}
                buildType={
                  build.romtype === BUILD_TYPE.GAPPS ? 'GApps' : 'Vanilla'
                }
                size={convertSizeToMb(parseInt(build.size))}
                url={build.url}
                version={build.version}
                md5={build.id}
                filename={build.filename}
              />
            ))
          ) : (
            <Flex width="100%" justifyContent="center" marginTop="1rem">
              <NoBuildsAvailableYet />
            </Flex>
          )}

          <Flex justifyContent="center" marginTop="1rem">
            <ArchiveButton />
          </Flex>
        </Flex>
      </Flex>

      <Flex
        id="DEVICE-deviceSpecs"
        direction="column"
        gap="1rem"
        padding="1rem"
        justifyContent="center"
        minWidth="350px"
        width="30%"
        alignContent="center"
        alignItems="center"
      >
        <Flex
          maxHeight={isMobile ? '520px !important' : '320px'}
          maxWidth="170px"
          justifyContent="center"
        >
          <img
            src={deviceImage}
            width="auto"
            height="100%"
            alt="smartphone-placeholder"
          />
        </Flex>

        <Box>
          <Text fontSize="large" fontWeight={600}>
            {`${deviceDetails?.name} (${deviceDetails?.codename})`}
          </Text>
        </Box>

        <Flex
          direction="column"
          gap="1rem"
          justifyContent="center"
          width="100%"
        >
          <Flex width="100%" direction="column">
            <Flex justifyContent="space-between" width="100%">
              <Box>
                <Text bold>{t(`${i18BasePath}.released`)}</Text>
              </Box>

              <Box>
                <Text>
                  {deviceSpecs?.release
                    ? releaseDate
                    : `${t(`${i18BasePath}.info-unavailable-short`)}`}
                </Text>
              </Box>
            </Flex>

            <Flex justifyContent="center" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.specifications`)}</Text>
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.soc`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="right">
                <Text>
                  {deviceSpecs?.soc ??
                    `${t(`${i18BasePath}.info-unavailable-short`)}`}
                </Text>
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.ram`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="right">
                <Text>
                  {deviceSpecs?.ram ??
                    `${t(`${i18BasePath}.info-unavailable-short`)}`}
                </Text>
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.cpu`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="right">
                <Text>
                  {deviceSpecs?.cpu
                    ? `${deviceSpecs?.cpu} ${deviceSpecs?.cpu_cores} @ ${deviceSpecs?.cpu_freq}`
                    : `${t(`${i18BasePath}.info-unavailable-short`)}`}
                </Text>
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.architecture`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="right">
                <Text>
                  {deviceSpecs?.architecture ??
                    `${t(`${i18BasePath}.info-unavailable-short`)}`}
                </Text>
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.gpu`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="right">
                <Text>
                  {deviceSpecs?.gpu ??
                    `${t(`${i18BasePath}.info-unavailable-short`)}`}
                </Text>
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.network`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="left">
                <ul style={{ color: 'white' }}>
                  {deviceSpecs?.network
                    ? deviceSpecs?.network?.map((item) => (
                        <li key={item}>
                          <Text>{item}</Text>
                        </li>
                      ))
                    : `${t(`${i18BasePath}.info-unavailable-short`)}`}
                </ul>
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.storage`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="right">
                <Text>
                  {deviceSpecs?.storage ??
                    `${t(`${i18BasePath}.info-unavailable-short`)}`}
                </Text>
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.sdcard`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="right">
                <Text>
                  {deviceSpecs?.sdcard ??
                    `${t(`${i18BasePath}.info-unavailable-short`)}`}
                </Text>
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.screen`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="right">
                {deviceSpecs?.screen ? (
                  <Text>
                    {deviceSpecs?.screen}
                    <br /> {deviceSpecs?.screen_res} ({deviceSpecs?.screen_ppi})
                    <br /> {deviceSpecs?.screen_tech}
                  </Text>
                ) : (
                  <Text>{`${t(`${i18BasePath}.info-unavailable-short`)}`}</Text>
                )}
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.bluetooth`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="right">
                {deviceSpecs?.bluetooth ? (
                  <>
                    <Text>v{deviceSpecs?.bluetooth?.spec}</Text>

                    <ul style={{ color: 'white' }}>
                      {deviceSpecs?.bluetooth?.profiles?.map((spec) => (
                        <li key={spec}>
                          <Text>{spec}</Text>
                        </li>
                      ))}
                    </ul>
                  </>
                ) : (
                  <Text>{`${t(`${i18BasePath}.info-unavailable-short`)}`}</Text>
                )}
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.peripherals`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="left">
                {deviceSpecs?.peripherals ? (
                  <ul style={{ color: 'white' }}>
                    {deviceSpecs?.peripherals?.map((peripheral) => (
                      <li key={peripheral}>
                        <Text>{peripheral}</Text>
                      </li>
                    ))}
                  </ul>
                ) : (
                  <Text>{`${t(`${i18BasePath}.info-unavailable-short`)}`}</Text>
                )}
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.cameras`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="right">
                {deviceSpecs?.cameras ? (
                  <>
                    <Text>{deviceSpecs?.cameras?.length}</Text>

                    <Box textAlign="left">
                      <ul style={{ color: 'white' }}>
                        {deviceSpecs?.cameras?.map((camera) => (
                          <li key={v4()}>
                            <Text>{`${camera.info}`}</Text>
                            <br />
                            <Text>{`${camera.flash}`}</Text>
                          </li>
                        ))}
                      </ul>
                    </Box>
                  </>
                ) : (
                  <Text>{`${t(`${i18BasePath}.info-unavailable-short`)}`}</Text>
                )}
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.dimensions`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="right">
                {deviceSpecs?.height ? (
                  <>
                    <Text>
                      {deviceSpecs?.height}
                      <br />
                    </Text>
                    <Text>
                      {deviceSpecs?.width}
                      <br />
                    </Text>
                    <Text>
                      {deviceSpecs?.depth}
                      <br />
                    </Text>
                  </>
                ) : (
                  <Text>{`${t(`${i18BasePath}.info-unavailable-short`)}`}</Text>
                )}
              </Box>
            </Flex>

            <Flex justifyContent="space-between" width="100%" marginTop="1rem">
              <Box>
                <Text bold>{t(`${i18BasePath}.battery`)}</Text>
              </Box>

              <Box maxWidth="60%" textAlign="right">
                {deviceSpecs?.battery ? (
                  <Text>
                    {`     ${
                      deviceSpecs?.battery?.removable
                        ? t(`${i18BasePath}.removable`)
                        : t(`${i18BasePath}.non-removable`)
                    }          ${deviceSpecs?.battery?.capacity} mAh`}
                    <br />

                    {`${deviceSpecs?.battery?.tech} ${t(
                      `${i18BasePath}.battery`
                    )}`}
                  </Text>
                ) : (
                  <Text>{`${t(`${i18BasePath}.info-unavailable-short`)}`}</Text>
                )}
              </Box>
            </Flex>
          </Flex>
        </Flex>
      </Flex>
    </Container>
  )
}

export default DeviceDetails
