import React from 'react'
import Box from '../src/components/Layout/Box'
import Button from '../src/components/Button'
import Container from '../src/components/Layout/Container'
import Flex from '../src/components/Layout/Flex'
import Logo from '../src/components/Logo'
import Text from '../src/components/Text'
import Image from 'next/image'
import Screenshot from '../public/img/screenshots/01.png'
import Card from '../src/components/Card'
import { v4 as uuidv4 } from 'uuid'
import HomeCards from '../src/constants/homeCards'
import Stock1 from '../public/img/stock/stock.webp'
import { useTranslation } from 'react-i18next'
import Link from 'next/link'
import ROUTES from '../src/constants/routes'
import { NAVBAR_LOGO_TYPE } from '../src/contracts/LogoProps'

const Home: React.FC = (): JSX.Element => {
  const cards = HomeCards({ image1: Stock1, image2: Stock1, image3: Stock1 })

  const { t } = useTranslation()

  return (
    <Container maxWidth="960px">
      <Flex justifyContent="center" alignItems="center" gap="5rem">
        <Flex
          direction="column"
          justifyContent="center"
          alignItems="flex-start"
        >
          <Flex
            direction="column"
            justifyContent="center"
            textAlign="center"
            paddingBottom="4rem"
          >
            <Logo type={NAVBAR_LOGO_TYPE.FULL} />
            <Text bold fontSize="h1">
              AOSPK
            </Text>
            <Text fontSize="h3">
              {t('pages.home.theBestAndroidExperience')}
            </Text>

            <Link href={ROUTES.DEVICES}>
              <Button fullWidth marginTop="25px">
                {t('pages.home.download')}
              </Button>
            </Link>
          </Flex>
        </Flex>

        <Flex
          id="HOME-phoneImage"
          className="slides"
          justifyContent="center"
          alignItems="center"
          alignContent="center"
        >
          <Image
            src={Screenshot}
            height={750}
            width={420}
            alt="screnshot"
            quality={100}
            priority
            placeholder="blur"
          />
        </Flex>
      </Flex>

      {/* Disabled for awhile */}

      {/* <Flex
        id="HOME-featureCardsContainer"
        padding="15px"
        justifyContent="space-between"
        gap="4rem"
      >
        {cards.map((card) => (
          <Box width="100%" key={uuidv4()}>
            <Card title={card.title} text={card.text} img={card.img} />
          </Box>
        ))}
      </Flex> */}
    </Container>
  )
}

export default Home
