// import { useRouter } from 'next/router'
import React from 'react'
import Box from '../src/components/Layout/Box'
import Flex from '../src/components/Layout/Flex'
import Text from '../src/components/Text'
import { useTranslation } from 'react-i18next'

const NotFound: React.FC = (): JSX.Element => {
  // const router = useRouter()
  const { t } = useTranslation()
  const i18BasePath = 'pages.404'

  return (
    <Flex
      alignContent="center"
      alignItems="center"
      justifyContent="center"
      minHeight="30rem"
      direction="column"
    >
      <Box textAlign="center">
        <Text bold fontSize="90px">
          404 | {t(`${i18BasePath}.notFound`)}
        </Text>
      </Box>

      <Box>
        <Text bold fontSize="40px">
          {t(`${i18BasePath}.takeMeHome`)}
        </Text>
      </Box>
    </Flex>
  )
}

export default NotFound
