import { createGlobalStyle, ThemeProps } from 'styled-components'
import darkTheme from './theme/dark'
import media from '../src/constants/mediaQueries'

const GlobalStyle = createGlobalStyle`

    /* @import url('https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,900;1,400;1,500;1,900&display=swap'); */

    :root {
      --max-container-width: 960px;
      scrollbar-color: ${(props: ThemeProps<typeof darkTheme>) =>
        props.theme.background.dark + props.theme.background.light}
    }

    #__next {
      height: 100%;
    }

    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
      font-family: 'Roboto', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto',
        'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
        sans-serif;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
    }

    html,
    body {
      background: ${(props) => props.theme.background.light} !important;
      width: 100%;
      height: 100%;
      outline: none;
    }

    span {
      font-weight: 600;
    }

    button {
      border: none;
    }

    button:focus {
      outline: none;
    }

    a {
      color: unset;
      text-decoration: none;
    }

    code {
      font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
        monospace;
    }

    ::-webkit-scrollbar-thumb {
      background-color: ${(props: ThemeProps<typeof darkTheme>) =>
        props.theme.background.dark}
    }

    ::-webkit-scrollbar-track {
      background: ${(props: ThemeProps<typeof darkTheme>) =>
        props.theme.background.light};
    }

    ::-webkit-scrollbar {
      width: 12px;
    }

    input::-webkit-input-placeholder {
      color: ${(props) => props.theme.foreground.dark};
    }

    input:-moz-placeholder {
      color: ${(props) => props.theme.foreground.dark};
    }

    input::-ms-input-placeholder  {
      color: ${(props) => props.theme.foreground.dark};
    }

    #HOME-featureCardsContainer {
      @media ${media.desktop} {
        width: 100%;
        flex-direction: column;
        justify-content: center;
      }
    }

    #HOME-phoneImage {
      @media ${media.desktop} {
        display: none;
      }
    }

    #DEVICE-deviceSpecsContainer {
      @media ${media.desktop} {
        flex-direction: column;
      }
    }

    #DEVICE-deviceSpecs {
      @media ${media.desktop} {
        width: 100%;
        height: auto;
        justify-content: center;
        align-items: center;
      }
    }

    #FOOTER-footerContainer {
      @media ${media.desktop} {
        flex-direction: column;
      }
    }

`

export default GlobalStyle
