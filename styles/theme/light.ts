const lightTheme = {
  background: '',
  foreground: '',
  highlight: {
    normal: '',
    alert: '',
    danger: '',
  },
}

export default lightTheme
