import { Theme } from 'styled-components'
import themeColors from './colors'

const darkTheme: Theme = {
  theme: 'dark',
  background: {
    dark: '#242526',
    light: '#2F363D',
    lighter: '#3C3C3C',
    paper: '#E4E4E4',
  },
  foreground: {
    dark: '#969696',
    light: '#ECEFF4',
  },
  highlight: {
    ...themeColors,
  },
  fontSize: {
    h1: '2em',
    h2: '1.5em',
    h3: '1.17em',
    h4: '1em',
    h5: '0.83em',
    h6: '0.67em',
    p: '1rem',
  },
}

export default darkTheme
