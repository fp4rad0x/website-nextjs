const themeColors = {
  logo: {
    light: '#00C389',
    dark: '#569092',
    blue: '#0089C3',
    highlight1: '#008db1',
    highlight2: '#00afb1',
    highlight3: '#00b2a8',
    highlight4: '#00b997',
    highlight5: '#00be8c',
    highlight6: '#00c57d',
    highlight7: '#00d4ff',
  },
  normal: '#ff0000',
  alert: '#ecff43',
  danger: '#ff4343',
}

export default themeColors
