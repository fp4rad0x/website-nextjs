enum BUILD_TYPE {
  VANILLA = 'vanilla',
  GAPPS = 'gapps',
}

export default BUILD_TYPE
