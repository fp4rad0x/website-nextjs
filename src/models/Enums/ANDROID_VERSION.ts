enum ANDROID_VERSION {
  ELEVEN = 'eleven',
  TWELVE = 'twelve',
}

export default ANDROID_VERSION
