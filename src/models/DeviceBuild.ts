export interface SingleBuild {
  datetime: string
  filename: string
  id: string
  size: string
  url: string
  romtype: 'vanilla' | 'gapps'
  version: '11' | '12'
}

interface DeviceBuild {
  eleven: {
    vanilla: {
      response: SingleBuild[] | []
    }
    gapps: {
      response: SingleBuild[] | []
    }
  }

  twelve: {
    vanilla: {
      response: SingleBuild[] | []
    }
    gapps: {
      response: SingleBuild[] | []
    }
  }
}

export default DeviceBuild
