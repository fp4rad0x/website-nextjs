export interface Battery {
  capacity: number
  removable: boolean
  tech: string
}

export interface Bluetooth {
  profiles: string[]
  spec: string
}

export interface Camera {
  flash: string
  info: string
}

export type NETWORK = '2G GSM' | '3G UMTS' | '4G LTE'

export default interface DeviceSpecs {
  architecture: string
  battery: Battery
  bluetooth: Bluetooth
  cameras: Camera[]
  codename: string
  cpu: string
  cpu_cores: string
  cpu_freq: string
  depth: string
  gpu: string
  height: string
  models: string[]
  name: string
  network: NETWORK[]
  peripherals: string[]
  ram: string
  release: string
  screen: string
  screen_ppi: string
  screen_res: string
  screen_tech: string
  sdcard: string
  soc: string
  storage: string
  vendor: string
  width: string
  wifi: string
}
