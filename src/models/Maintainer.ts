type DeviceMaintainerCodename = {
  codename: string
}

interface Maintainer {
  name: string
  country: string
  github_username: string
  telegram_username: string
  xda_url: string
  devices: DeviceMaintainerCodename[]
  ci_username: 'tioburla'
}

export default Maintainer
