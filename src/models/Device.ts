type AndroidVersions = 'eleven' | 'twelve'
type Gapps = 'gapps' | 'vanilla'

interface Device {
  brand: string
  name: string
  codename: string
  xda_thread: string
  supported_versions: AndroidVersions[]
  supported_types: Gapps[]
  deprecated: boolean
  repositories: string[]
}

export default Device
