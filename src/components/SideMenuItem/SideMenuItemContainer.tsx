import styled from 'styled-components'
import SideMenuItemContainerProps from '../../contracts/SideMenuItemContainerProps'

const SideMenuItemContainer = styled.div<SideMenuItemContainerProps>`
  display: flex;
  color: ${(props) =>
    props.theme === 'dark'
      ? props.theme.foreground.light
      : props.theme.background.light};
`

export default SideMenuItemContainer
