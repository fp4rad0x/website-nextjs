import React from 'react'
import SideMenuItemContainer from './SideMenuItemContainer'
import SideMenuItemProps from '../../contracts/SideMenuItemProps'
import Flex from '../Layout/Flex'
import Text from '../Text'

const SideMenuItem: React.FC<SideMenuItemProps> = (props): JSX.Element => {
  const { icon } = props

  return (
    <SideMenuItemContainer>
      <Flex gap="0.5rem" justifyContent="flex-start" alignItems="center">
        {icon ? icon : <></>}
        <Text>Device name</Text>
      </Flex>
    </SideMenuItemContainer>
  )
}

export default SideMenuItem
