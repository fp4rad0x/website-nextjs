import React from 'react'
import Flex from '../Layout/Flex'
import { FcCancel } from 'react-icons/fc'
import Box from '../Layout/Box'
import Text from '../Text'
import useIsMobile from '../../hooks/useIsMobile'
import { useTranslation } from 'react-i18next'

const NoBuildsAvailableYet = (): JSX.Element => {
  const isMobile = useIsMobile()
  const { t } = useTranslation()

  return (
    <Flex
      gap="1rem"
      direction={isMobile ? 'column' : 'row'}
      justifyContent="center"
      alignItems="center"
      alignContent="center"
    >
      <Box>
        <FcCancel size="4rem" />
      </Box>

      <Box>
        <Text fontSize="x-large">{t('pages.device.noBuildsHereYet')}</Text>
      </Box>
    </Flex>
  )
}

export default NoBuildsAvailableYet
