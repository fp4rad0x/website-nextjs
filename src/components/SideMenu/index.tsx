import React from 'react'
import Flex from '../Layout/Flex'
import SideMenuItem from '../SideMenuItem'
import SideMenuContainer from './SideMenuContainer'
import { GoDeviceMobile } from 'react-icons/go'

const SideMenu: React.FC = (): JSX.Element => {
  return (
    <SideMenuContainer>
      <Flex>
        <SideMenuItem icon={<GoDeviceMobile />} />
      </Flex>
    </SideMenuContainer>
  )
}

export default SideMenu
