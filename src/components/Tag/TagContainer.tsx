import styled from 'styled-components'

const TagContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  padding: 4px;
  border-radius: 4px;

  /* background: ${(props) => props.theme.highlight.danger}; */
  /* color: ${(props) => props.theme.foreground.light}; // TODO - Add dark fg color */
`

export default TagContainer
