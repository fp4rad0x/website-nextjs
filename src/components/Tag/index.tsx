import React, { useContext } from 'react'
import TagProps from '../../contracts/TagProps'
import Flex from '../Layout/Flex'
import Box from '../Layout/Box'
import Text from '../Text'
import { TiWarning, TiTick, TiInfoLarge, TiDelete } from 'react-icons/ti'
import themeColors from '../../../styles/theme/colors'
import Tippy from '@tippyjs/react'
import { ThemeContext } from 'styled-components'

const Tag: React.FC<TagProps> = (props): JSX.Element => {
  const { variant, text, toolTip } = props

  const theme = useContext(ThemeContext)

  return (
    <Flex gap="5px">
      <Tippy
        content={toolTip}
        theme={theme.theme === 'dark' ? 'light' : 'material'}
      >
        <Box>
          {variant === 'warning' && (
            <TiWarning color={props.color ?? themeColors.alert} />
          )}

          {variant === 'danger' && (
            <TiDelete color={props.color ?? themeColors.danger} />
          )}

          {variant === 'info' && (
            <TiInfoLarge color={props.color ?? themeColors.logo.blue} />
          )}

          {variant === 'ok' && (
            <TiTick color={props.color ?? themeColors.logo.light} />
          )}
        </Box>
      </Tippy>

      <Box>{text ?? <Text>{text}</Text>}</Box>
    </Flex>
  )
}

export default Tag
