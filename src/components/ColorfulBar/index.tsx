import styled from 'styled-components'

const ColorfulBar = styled.div`
  height: 6px;
  width: 100%;
  background: linear-gradient(
    90deg,
    rgba(0, 141, 177, 1) 0%,
    rgba(0, 175, 177, 1) 11%,
    rgba(0, 178, 168, 1) 24%,
    rgba(0, 185, 151, 1) 36%,
    rgba(0, 190, 140, 1) 56%,
    rgba(0, 197, 125, 1) 78%,
    rgba(0, 212, 255, 1) 100%
  );

  /* background: ${(props) => `linear-gradient(
    90deg,
    ${props.theme.highlight.logo.highlight1} 0%,
    ${props.theme.highlight.logo.highlight2} 11%,
    ${props.theme.highlight.logo.highlight3} 24%,
    ${props.theme.highlight.logo.highlight4} 36%,
    ${props.theme.highlight.logo.highlight5} 56%,
    ${props.theme.highlight.logo.highlight6} 78%,
    ${props.theme.highlight.logo.highlight7} 100%,
  )`}; */

  background-color: ${(props) => props.theme.highlight.logo.lightlight1};
`

export default ColorfulBar
