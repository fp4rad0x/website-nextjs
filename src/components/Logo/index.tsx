import React from 'react'
import LogoProps from '../../contracts/LogoProps'
import Text from '../Text'
import Flex from '../Layout/Flex'
import LogoContainer from './LogoContainer'

const Logo: React.FC<LogoProps> = (props): JSX.Element => {
  const { text, type, divider, onClick, cursor } = props

  return (
    <>
      <LogoContainer
        divider={divider}
        type={type}
        onClick={onClick}
        cursor={cursor}
      >
        <Flex alignItems="center">
          <img
            alt="AOSPK Logo"
            src="/logo/logo-small-250x360.png"
            height={type === 'NavBar' ? '35px' : '300px'}
          />
        </Flex>

        {text ? (
          <Flex
            direction="column"
            justifyContent="flex-start"
            width="max-content"
            cursor="pointer"
            marginRight={text && '1rem'}
          >
            <Text bold>AOSPK</Text>
            <Text>The Kraken Project</Text>
          </Flex>
        ) : (
          <></>
        )}
      </LogoContainer>
    </>
  )
}

export default Logo
