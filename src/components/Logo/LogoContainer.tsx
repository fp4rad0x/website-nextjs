import styled from 'styled-components'
import LogoProps from '../../contracts/LogoProps'

const LogoContainer = styled.div<LogoProps>`
  display: flex;
  color: ${(props) => props.theme.foreground.light};
  gap: 1rem;
  justify-content: ${(props) =>
    props.type === 'fullWidth' ? 'center' : 'unset'};
  border-right: ${(props) =>
    props.divider ? `3px solid ${props.theme.foreground.light}15` : 'none'};
  cursor: ${(props) => (props.cursor ? props.cursor : 'initial')};
`

export default LogoContainer
