import React from 'react'
import Flex from '../Layout/Flex'
import Paper from '../Paper'
import Text from '../Text'
import Button from '../Button'
import DeviceCardProps from '../../contracts/DeviceCardProps'
import { GoDeviceMobile } from 'react-icons/go'
import { FaExternalLinkAlt } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import Tag from '../Tag'
import Link from 'next/link'
import router from 'next/router'
import ROUTES from '../../constants/routes'

const DeviceCard: React.FC<DeviceCardProps> = (props): JSX.Element => {
  const { brand, deviceName, deviceCodeName, deprecated } = props

  const { t } = useTranslation()
  const i18BasePath = 'pages.devices'

  return (
    <Paper
      padding="15px !important"
      minHeight="180px"
      minWidth="300px"
      maxWidth="300px"
      justifyContent="space-between"
      direction="column"
    >
      <Flex
        direction="column"
        justifyContent="space-between"
        width="100%"
        height="100%"
      >
        <Flex justifyContent="space-between" alignItems="center">
          <Flex
            gap="10px"
            justifyContent="flex-start"
            alignItems="center"
            marginRight="10px"
          >
            <GoDeviceMobile />
            <Text bold fontSize="1.2rem">
              {deviceName}
            </Text>
          </Flex>

          <Flex justifyContent="flex-start" alignItems="center">
            <Tag
              variant={deprecated ? 'warning' : 'ok'}
              toolTip={
                deprecated
                  ? t(`${i18BasePath}.deprecated`)
                  : t(`${i18BasePath}.activelyMaintained`)
              }
            />
          </Flex>
        </Flex>

        <Flex direction="column">
          <Text>{brand}</Text>
          <Flex gap="5px">
            <Text>{t(`${i18BasePath}.codename`)}:</Text>
            <Text color="darken">{deviceCodeName}</Text>
          </Flex>
        </Flex>
      </Flex>

      <Flex marginTop="10px" gap="20px" width="100%">
        {/* <Link href={`/devices/${deviceCodeName}`}> */}
        <Button
          fullWidth
          gap="0.8rem"
          onClick={() => router.push(`${ROUTES.DEVICES}/${deviceCodeName}`)}
        >
          {t(`${i18BasePath}.downloadButton`)}
          <FaExternalLinkAlt />
        </Button>
      </Flex>
    </Paper>
  )
}

export default DeviceCard
