import React from 'react'
import BrandFilterContainer from './BrandFilterContainer'
import Text from '../Text'
import BrandFilterProps from '../../contracts/BrandFilterProps'

const BrandFilter: React.FC<BrandFilterProps> = (props): JSX.Element => {
  const { label, onClick, active } = props

  return (
    <BrandFilterContainer active={active} onClick={onClick}>
      <Text fontWeight={500}>{`# ${label}` ?? '# brandName'}</Text>
    </BrandFilterContainer>
  )
}

export default BrandFilter
