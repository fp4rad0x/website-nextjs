import styled from 'styled-components'
import { darken } from 'polished'
import BrandFilterContainerProps from '../../contracts/BrandFilterContainerProps'

const BrandFilter = styled.div<BrandFilterContainerProps>`
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  background-color: ${(props) =>
    props.active
      ? props.theme.highlight.logo.light
      : props.theme.background.dark};
  border-radius: 3px;
  padding: 5px 10px;
  transition: 150ms;

  &:hover {
    transition: 150ms;
    background-color: ${(props) =>
      props.active
        ? darken(0.05, props.theme.highlight.logo.light)
        : darken(0.05, props.theme.background.dark)};
  }
`

export default BrandFilter
