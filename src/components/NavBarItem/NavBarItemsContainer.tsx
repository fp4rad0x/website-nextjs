import styled from 'styled-components'

const NavBarItemContainer = styled.div`
  align-items: center;
  width: max-content;
  display: flex;
  justify-content: space-between;
  gap: 1rem;
`

export default NavBarItemContainer
