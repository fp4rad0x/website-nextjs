import styled from 'styled-components'

type NavBarItemProps = {
  fontSize?: string
}

const NavBarItem = styled.div`
  display: flex;
  font-weight: 500;
  cursor: pointer;
  color: ${(props) => props.theme.foreground.dark};
  transition: 500ms;
  font-size: ${(props: NavBarItemProps) =>
    props.fontSize ? props.fontSize : 'unset'};

  &:hover {
    transition: 500ms;
    color: ${(props) =>
      props.color ? props.color : props.theme.highlight.logo.light};
  }
`

export default NavBarItem
