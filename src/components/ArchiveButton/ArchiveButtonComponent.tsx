import { darken } from 'polished'
import styled from 'styled-components'

const ArchiveButtonComponent = styled.button`
  padding: 5px;
  border-radius: 4px;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  gap: 5px;
  font-size: 1rem;
  color: ${(props) => props.theme.foreground.light};
  font-weight: bold;
  background-color: ${(props) => props.theme.highlight.logo.blue};
  transition: 200ms;

  &:hover {
    transition: 200ms;
    background-color: ${(props) =>
      darken(0.05, props.theme.highlight.logo.blue)};
  }

  &:active {
    transition: 200ms;
    background-color: ${(props) =>
      darken(0.135, props.theme.highlight.logo.blue)};
  }
`

export default ArchiveButtonComponent
