import React from 'react'
import ArchiveButtonComponent from './ArchiveButtonComponent'
import { FaFileArchive } from 'react-icons/fa'

const ArchiveButton: React.FC = (props): JSX.Element => {
  return (
    <ArchiveButtonComponent {...props}>
      <FaFileArchive />
      Archive
    </ArchiveButtonComponent>
  )
}

export default ArchiveButton
