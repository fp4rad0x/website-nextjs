import styled from 'styled-components'
import PaperProps from '../../contracts/PaperProps'

const Paper = styled.div<PaperProps>`
  border-radius: ${(props) =>
    props.borderRadius ? props.borderRadius : '6px'};
  /* background-color: #94AABA; */
  background-color: ${(props) => props.theme.background.dark};
  color: ${(props) => props.theme.foreground.light};
  /* ${(props) =>
    props.backgroundColor
      ? props.backgroundColor
      : props.theme.foreground.light}; */

  display: flex;
  flex-direction: ${(props) => (props.direction ? props.direction : 'row')};
  justify-content: ${(props) => props.justifyContent && props.justifyContent};
  align-content: ${(props) => props.alignContent && props.alignContent};

  flex-wrap: ${(props) => props.flexWrap && props.flexWrap};

  align-items: ${(props) => props.alignItems && props.alignItems};
  gap: ${(props) => props.gap && props.gap};

  height: ${(props) => props.height && props.height};
  width: ${(props) => props.width && props.width};

  min-height: ${(props) => props.minHeight && props.minHeight};
  min-width: ${(props) => props.minWidth && props.minWidth};

  max-height: ${(props) => props.maxHeight && props.maxHeight};
  max-width: ${(props) => props.maxWidth && props.maxWidth};

  padding: ${(props) => props.padding && props.padding};
  padding-top: ${(props) => props.paddingTop && props.paddingTop};
  padding-bottom: ${(props) => props.paddingBottom && props.paddingBottom};
  padding-right: ${(props) => props.paddingRight && props.paddingRight};
  padding-left: ${(props) => props.paddingLeft && props.paddingLeft};

  margin: ${(props) => props.margin && props.margin};
  margin-top: ${(props) => props.marginTop && props.marginTop};
  margin-bottom: ${(props) => props.paddingBottom && props.paddingBottom};
  margin-right: ${(props) => props.marginRight && props.marginRight};
  margin-left: ${(props) => props.marginLeft && props.marginLeft};

  text-align: ${(props) => props.textAlign && props.textAlign};

  cursor: ${(props) => props.cursor && props.cursor};
`
export default Paper
