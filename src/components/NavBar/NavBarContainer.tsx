import styled from 'styled-components'

const NavBarContainer = styled.div`
  background-color: ${(props) => props.theme.background.dark};
`

export default NavBarContainer
