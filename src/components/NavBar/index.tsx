import React, { useState } from 'react'
import Logo from '../Logo'
import NavBarItem from '../NavBarItem'
import NavBarItemsContainer from '../NavBarItem/NavBarItemsContainer'
import NavBarContainer from './NavBarContainer'
import { AiFillGithub } from 'react-icons/ai'
import Container from '../Layout/Container'
import NavBarProps from '../../contracts/NavBarProps'
import { useRouter } from 'next/router'
import Flex from '../Layout/Flex'
import ROUTES from '../../constants/routes'
import ColorfulBar from '../ColorfulBar'
import Link from 'next/link'
import LINKS from '../../constants/links'
import HamburgerMenuIcon from '../HamburgerMenu/Icon'
import useIsMobile from '../../hooks/useIsMobile'
import Box from '../Layout/Box'
import { MOBILE_MENU_WIDTH } from '../../constants/constants'
import { NAVBAR_LOGO_TYPE } from '../../contracts/LogoProps'

const NavBar: React.FC<NavBarProps> = (props): JSX.Element => {
  const { NavBarItems, menuActive, setMenuActive } = props

  const router = useRouter()
  const isMobile = useIsMobile(MOBILE_MENU_WIDTH)

  return (
    <>
      <NavBarContainer>
        <ColorfulBar />
        <Container
          maxWidth="960px"
          flex
          alignItems="center"
          gap="1rem"
          padding="1rem"
          paddingRight="1rem"
          justifyContent="space-between"
        >
          {isMobile ? (
            <Flex justifyContent="space-between" width="100%">
              <Box>
                <HamburgerMenuIcon
                  menuActive={menuActive}
                  onClick={() => setMenuActive((prev) => !prev)}
                />
              </Box>

              <Box>
                <Logo
                  type={NAVBAR_LOGO_TYPE.NAVBAR}
                  onClick={() => router.push(ROUTES.HOME)}
                  cursor="pointer"
                />
              </Box>
            </Flex>
          ) : (
            <>
              <Logo
                type={NAVBAR_LOGO_TYPE.NAVBAR}
                divider
                text
                onClick={() => router.push(ROUTES.HOME)}
                cursor="pointer"
              />

              <Flex justifyContent="space-between" width="100%">
                <NavBarItemsContainer>
                  {NavBarItems.map((item) => (
                    <NavBarItem
                      key={item.label}
                      onClick={() => {
                        router.push(item.link)
                      }}
                    >
                      {item.label}
                    </NavBarItem>
                  ))}
                </NavBarItemsContainer>

                <NavBarItemsContainer>
                  <NavBarItem fontSize="2rem">
                    <Link href={LINKS.GITHUB}>
                      <AiFillGithub />
                    </Link>
                  </NavBarItem>
                </NavBarItemsContainer>
              </Flex>
            </>
          )}
        </Container>
      </NavBarContainer>
    </>
  )
}

export default NavBar
