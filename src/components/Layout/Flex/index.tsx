/* eslint-disable sonarjs/no-identical-expressions */
import styled from 'styled-components'
import FlexProps from '../../../contracts/FlexProps'

const Flex = styled.div<FlexProps>`
  max-width: ${(props) => props.maxWidth && props.maxWidth};
  min-width: ${(props) => props.minWidth && props.minWidth};
  max-height: ${(props) => props.maxHeight && props.maxHeight};

  display: flex;
  flex-direction: ${(props) => (props.direction ? props.direction : 'row')};
  justify-content: ${(props) => props.justifyContent && props.justifyContent};
  align-content: ${(props) => props.alignContent && props.alignContent};

  flex-wrap: ${(props) => props.flexWrap && props.flexWrap};

  align-items: ${(props) => props.alignItems && props.alignItems};
  gap: ${(props) => props.gap && props.gap};

  height: ${(props) => props.height && props.height};
  min-height: ${(props) => props.minHeight && props.minHeight};
  width: ${(props) => props.width && props.width};

  padding: ${(props) => props.padding && props.padding};
  padding-top: ${(props) => props.paddingTop && props.paddingTop};
  padding-bottom: ${(props) => props.paddingBottom && props.paddingBottom};
  padding-right: ${(props) => props.paddingRight && props.paddingRight};
  padding-left: ${(props) => props.paddingLeft && props.paddingLeft};

  margin: ${(props) => props.margin && props.margin};
  margin-top: ${(props) => props.marginTop && props.marginTop};
  margin-bottom: ${(props) => props.paddingBottom && props.paddingBottom};
  margin-right: ${(props) => props.marginRight && props.marginRight};
  margin-left: ${(props) => props.marginLeft && props.marginLeft};

  text-align: ${(props) => props.textAlign && props.textAlign};

  cursor: ${(props) => props.cursor && props.cursor};

  background-color: ${(props) =>
    props.backgroundColor && props.backgroundColor};
`

export default Flex
