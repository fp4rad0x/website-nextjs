/* eslint-disable sonarjs/no-identical-expressions */
import styled from 'styled-components'
import BoxProps from '../../../contracts/BoxProps'

const Box = styled.div<BoxProps>`
  height: ${(props) => props.height && props.height};
  width: ${(props) => props.width && props.width};
  max-width: ${(props) => props.maxWidth && props.maxWidth};

  background: ${(props) => props.background && props.background};

  top: ${(props) => props.top && props.top};
  bottom: ${(props) => props.bottom && props.bottom};
  right: ${(props) => props.right && props.right};
  left: ${(props) => props.left && props.left};

  background-color: ${(props) =>
    props.backgroundColor && props.backgroundColor};

  position: ${(props) => props.position && props.position};

  padding: ${(props) => props.padding && props.padding};
  padding-top: ${(props) => props.paddingTop && props.paddingTop};
  padding-bottom: ${(props) => props.paddingBottom && props.paddingBottom};
  padding-right: ${(props) => props.paddingRight && props.paddingRight};
  padding-left: ${(props) => props.paddingLeft && props.paddingLeft};

  margin: ${(props) => props.margin && props.padding};
  margin-top: ${(props) => props.marginTop && props.marginTop};
  margin-bottom: ${(props) => props.marginBottom && props.marginBottom};
  margin-right: ${(props) => props.marginRight && props.marginRight};
  margin-left: ${(props) => props.marginLeft && props.marginLeft};

  text-align: ${(props) => props.textAlign && props.textAlign};

  cursor: ${(props) => props.cursor && props.cursor};
`

export default Box
