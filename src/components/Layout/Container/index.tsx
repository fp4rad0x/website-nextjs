import styled from 'styled-components'
import ContainerProps from '../../../contracts/ContainerProps'

const Container = styled.div<ContainerProps>`
  background-color: ${(props) =>
    props.backgroundColor && props.backgroundColor};

  height: ${(props) => props.height && props.height};
  max-height: ${(props) => props.maxHeight && props.maxHeight};
  min-height: ${(props) => props.minHeight && props.minHeight};

  width: ${(props) => props.width && props.width};
  min-width: ${(props) => props.minWidth && props.minWidth};

  margin-left: auto;
  margin-right: auto;
  gap: ${(props) => props.gap && props.gap};
  justify-content: ${(props) => props.justifyContent && props.justifyContent};
  display: ${(props) => (props.flex ? 'flex' : 'block')};
  flex-direction: ${(props) =>
    props.flexDirection ? props.flexDirection : 'row'};
  max-width: ${(props) => props.maxWidth && props.maxWidth};
  align-items: ${(props) => props.alignItems && props.alignItems};
  padding: ${(props): string => (props.padding ? props.padding : '15px')};

  padding: ${(props) => props.padding && props.padding};
  padding-top: ${(props) => props.paddingTop && props.paddingTop};
  padding-bottom: ${(props) => props.paddingBottom && props.paddingBottom};
  padding-right: ${(props) => props.paddingRight && props.paddingRight};
  padding-left: ${(props) => props.paddingLeft && props.paddingLeft};
`

export default Container
