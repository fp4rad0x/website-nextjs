import React, { useEffect, useState } from 'react'
import Flex from '../Layout/Flex'
import Paper from '../Paper'
import Text from '../Text'
import Button from '../Button'
import { FaDownload } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import Box from '../Layout/Box'
import BuildCardProps from '../../contracts/BuildCardProps'

const BuildCard: React.FC<Partial<BuildCardProps>> = (props): JSX.Element => {
  const { buildType, size, md5, version, filename, url } = props

  const [deviceBuildDate, setDeviceBuildDate] = useState('')

  const { t } = useTranslation()
  const i18BasePath = 'pages.devices'

  const getBuildDateFromFilename = (filename: string): string => {
    if (!filename?.length) {
      return ''
    }

    const dateTimeRegex = /(\d{8})-(\d{4})/
    const dateTime = filename?.match(dateTimeRegex)

    const date = dateTime?.[1]
    const dateString = date?.replace(/^(\d{4})(\d{2})(\d{2})$/, '$1-$2-$3')

    return dateString ?? ''
  }

  useEffect(() => {
    const buildDate = getBuildDateFromFilename(filename ?? '')
    setDeviceBuildDate(buildDate)
  }, [])

  return (
    <Paper
      padding="15px !important"
      minHeight="180px"
      minWidth="300px"
      width="100%"
      justifyContent="space-between"
      direction="column"
    >
      <Flex
        direction="column"
        justifyContent="space-between"
        width="100%"
        height="100%"
      >
        <Flex gap="10px" justifyContent="center">
          <Text fontWeight={500} fontSize="1.5rem">
            {buildType} build
          </Text>
        </Flex>

        <Flex direction="column">
          <Flex gap="0.5rem" direction="column">
            <Box>
              <Text fontSize="smaller">
                <Text bold>{t(`${i18BasePath}.size`)}: </Text>
                {size} mb
              </Text>
            </Box>

            <Box>
              <Text fontSize="smaller">
                <Text bold>Md5 hash: </Text>
                {md5}
              </Text>
            </Box>

            <Box>
              <Text fontSize="smaller">
                <Text bold>Filename: </Text>
                {filename}
              </Text>
            </Box>

            <Box>
              <Text fontSize="smaller">
                <Text bold>Version: </Text>
                {version}
              </Text>
            </Box>

            <Box>
              <Text fontSize="smaller">
                <Text bold>Date: </Text>
                {deviceBuildDate ?? 'Build date unavailable'}
              </Text>
            </Box>
          </Flex>
        </Flex>
      </Flex>

      <Flex marginTop="10px" gap="20px" width="100%">
        <Button fullWidth gap="0.8rem">
          <a href={url} target="_blank" rel="noreferrer">
            {t(`${i18BasePath}.downloadButton`)} <FaDownload />
          </a>
        </Button>
      </Flex>
    </Paper>
  )
}

export default BuildCard
