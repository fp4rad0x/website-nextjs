import styled, { css } from 'styled-components'
import { TextProps, VARIANT } from '../../contracts/TextProps'

const Text = styled.span<TextProps>`
  font-weight: ${(props) =>
    props.bold
      ? '800 !important'
      : props.fontWeight
      ? props.fontWeight
      : '400 !important'};
  font-size: ${(props) => props.fontSize && props.fontSize};
  opacity: ${(props) => (props.opacity ? props.opacity : '100%')};

  color: ${(props) => {
    const hex = new RegExp(/^#.*/)

    if (props.color === 'darken') {
      return props.theme.foreground.dark
    }

    if (String(props.color).match(hex)) {
      return props.color
    }

    return props.theme.foreground.light
  }};

  ${(props) => {
    if (!props.variant) {
      return
    }

    return css`
      font-size: ${props.theme.fontSize[props.variant || VARIANT.p]};
    `
  }}

  &:hover {
    cursor: ${(props) => props.cursor};
  }
`

export default Text
