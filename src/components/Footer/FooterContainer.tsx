import styled from 'styled-components'

const FooterContainer = styled.div`
  height: 180px;
  max-height: 180px;
  padding: 20px;
  margin-top: 35px;
  background-color: ${(props) => props.theme.background.dark};
  color: ${(props) => props.theme.foreground.light};
`

export default FooterContainer
