import React from 'react'
import Container from '../Layout/Container'
import Flex from '../Layout/Flex'
import Text from '../Text'
import FooterContainer from './FooterContainer'
import { v4 as uuidv4 } from 'uuid'
import useNavBarItems from '../../hooks/useNavBarItems'
import FooterIcon from '../FooterIcon'
import { useRouter } from 'next/router'
import { AiFillGithub } from 'react-icons/ai'
import { FaCode, FaPaypal, FaTelegram } from 'react-icons/fa'
import { BiDonateHeart } from 'react-icons/bi'
import Link from 'next/link'
import { useTranslation } from 'react-i18next'
import LINKS from '../../constants/links'
import Box from '../Layout/Box'

const Footer: React.FC = (): JSX.Element => {
  const footerItems = useNavBarItems()
  const router = useRouter()
  const { t } = useTranslation()

  const iconSize = 20

  const i18BasePath = 'footer'

  const footerIcons = [
    {
      name: 'Telegram',
      link: LINKS.TELEGRAM,
      icon: <FaTelegram size={iconSize} />,
      description: t(`${i18BasePath}.telegramIcon`),
    },
    {
      name: 'Github',
      link: LINKS.GITHUB,
      icon: <AiFillGithub size={iconSize} />,
      description: t(`${i18BasePath}.githubIcon`),
    },
    {
      name: 'Source Code',
      link: LINKS.GITHUB,
      icon: <FaCode size={iconSize} />,
      description: t(`${i18BasePath}.sourceCodeIcon`),
    },
    {
      name: 'Donation',
      link: LINKS.DONATION,
      icon: <BiDonateHeart size={iconSize} />,
      description: t(`${i18BasePath}.donationIcon`),
    },
    {
      name: 'PayPal',
      link: LINKS.PAYPAL,
      icon: <FaPaypal size={iconSize} />,
      description: t(`${i18BasePath}.paypalIcon`),
    },
  ]

  return (
    <FooterContainer>
      <Container
        maxWidth="960px"
        flex
        flexDirection="column"
        gap="2rem"
        justifyContent="space-between"
      >
        <Box>
          <Flex id="FOOTER-footerContainer" justifyContent="space-between" gap="2rem" alignItems="center">
            <Flex>
              {footerItems.map((item, index) => (
                <Flex key={uuidv4()} gap="5px" marginRight="5px">
                  <Text
                    fontWeight={500}
                    onClick={() => router.push(item.link)}
                    cursor="pointer"
                  >
                    {item.label}
                  </Text>
                  {index !== footerItems.length - 1 && (
                    <Text opacity="30%">/</Text>
                  )}
                </Flex>
              ))}
            </Flex>

            <Flex gap="1rem">
              {footerIcons.map((icon) => (
                <Link href={icon.link} key={icon.name}>
                  <a target="_blank">
                    <FooterIcon toolTip={icon.description}>
                      {icon.icon}
                    </FooterIcon>
                  </a>
                </Link>
              ))}
            </Flex>
          </Flex>
        </Box>

        <Flex textAlign="center" justifyContent="center" direction="column">
          <Text>Copyright 2021 @ The Kraken Project</Text>
        </Flex>
      </Container>
    </FooterContainer>
  )
}

export default Footer
