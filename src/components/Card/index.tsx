import React from 'react'
import CardProps from '../../contracts/CardProps'
import Box from '../Layout/Box'
import Flex from '../Layout/Flex'
import Image from 'next/image'
import { v4 as uuidv4 } from 'uuid'
import Text from '../Text'

const Card: React.FC<CardProps> = (props): JSX.Element => {
  const { title, text, img } = props

  return (
    <Flex direction="column" padding="5px" gap="15px" justifyContent="center">
      <Box textAlign="center">
        <Image
          src={img}
          height={190}
          width={190}
          alt={`stock-v${uuidv4()}`}
          placeholder="blur"
        />
      </Box>

      <Flex textAlign="center" direction="column" maxWidth="24rem">
        <Box>
          <Text bold fontSize="h2">
            {title}
          </Text>
        </Box>

        <Box>
          <Text fontSize="h4">{text}</Text>
        </Box>
      </Flex>
    </Flex>
  )
}

export default Card
