import React, { useContext } from 'react'
import FooterIconProps from '../../contracts/FooterIconProps'
import FooterIconContainer from './FooterIconContainer'
import Tippy from '@tippyjs/react'
import { ThemeContext } from 'styled-components'

const FooterIcon: React.FC<FooterIconProps> = ({
  onClick,
  children,
  toolTip
}): JSX.Element => {
  const theme = useContext(ThemeContext)

  return (
    <Tippy
      content={toolTip}
      theme={theme.theme === 'dark' ? 'light' : 'material'}
    >
      <FooterIconContainer onClick={onClick}>{children}</FooterIconContainer>
    </Tippy>
  )
}

export default FooterIcon
