import styled from 'styled-components'

const FooterIconContainer = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${(props) => props.theme.background.light};
  border-radius: 5px;
  padding: 3px;
`

export default FooterIconContainer
