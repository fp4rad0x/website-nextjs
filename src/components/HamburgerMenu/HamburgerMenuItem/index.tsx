import styled from 'styled-components'

const HamburgerMenuItem = styled.div`
  color: ${(props) => props.theme.foreground.light};
  font-size: 2rem;
  font-weight: bold;
  text-align: center;
  cursor: pointer;
  transition: 300ms;

  &:hover {
    color: ${(props) => props.theme.highlight.logo.light};
    transition: 300ms;
  }
`

export default HamburgerMenuItem
