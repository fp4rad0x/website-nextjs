import React from 'react'
import HamburgerMenuProps from '../../../contracts/HamburgerMenuProps'
import useNavBarItems from '../../../hooks/useNavBarItems'
import HamburgerMenuItem from '../HamburgerMenuItem'
import HamburgerMenuContainer from './HamburgerMenuContainer'
import { useRouter } from 'next/router'
import ROUTES from '../../../constants/routes'
import Box from '../../Layout/Box'

const HamburgerMenu: React.FC<HamburgerMenuProps> = (props): JSX.Element => {
  const { isActive, setIsActive } = props

  const NavBarItems = useNavBarItems()
  const router = useRouter()

  // const currentRoute = router.pathname

  return (
    <HamburgerMenuContainer
      id="MENU-container"
      left={isActive ? '0' : '-300vw'}
    >
      <Box marginBottom="1rem">
        <HamburgerMenuItem
          onClick={() => {
            setIsActive(false)
            router.push(ROUTES.HOME)
          }}
        >
          {'Home'}
        </HamburgerMenuItem>
        <hr />
      </Box>

      {NavBarItems.map((item) => (
        <HamburgerMenuItem
          key={item.label}
          onClick={() => {
            setIsActive(false)
            router.push(item.link)
          }}
        >
          {item.label}
        </HamburgerMenuItem>
      ))}
    </HamburgerMenuContainer>
  )
}

export default HamburgerMenu
