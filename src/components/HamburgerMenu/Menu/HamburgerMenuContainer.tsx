import styled from 'styled-components'
import HamburgerMenuContainerProps from '../../../contracts/HamburgerMenuContainerProps'

const HamburgerMenuContainer = styled.div<HamburgerMenuContainerProps>`
  width: 100%;
  height: 100%;
  padding: 2rem;
  position: absolute;
  background-color: ${(props) => props.theme.background.dark};
  left: ${(props) => props.left && props.left};
  transition: 200ms;
  top: 75px;
  height: calc(100% - 75px);
  z-index: 99999;
`

export default HamburgerMenuContainer
