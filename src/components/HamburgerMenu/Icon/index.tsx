import React from 'react'
import { GiHamburgerMenu } from 'react-icons/gi'
import { IoMdClose } from 'react-icons/io'
import { useContext } from 'react'
import { ThemeContext } from 'styled-components'
import Box from '../../Layout/Box'
import HamburgerMenuIconProps from '../../../contracts/HamburgerMenuIconProps'

const HamburgerMenuIcon: React.FC<HamburgerMenuIconProps> = (
  props
): JSX.Element => {
  const { onClick, menuActive } = props

  const theme = useContext(ThemeContext)

  return (
    <div>
      <Box cursor="pointer" onClick={onClick}>
        {menuActive ? (
          <IoMdClose
            color={
              theme.theme === 'dark'
                ? theme.foreground.light
                : theme.foreground.dark
            }
            size="2rem"
          />
        ) : (
          <GiHamburgerMenu
            color={
              theme.theme === 'dark'
                ? theme.foreground.light
                : theme.foreground.dark
            }
            size="2rem"
          />
        )}
      </Box>
    </div>
  )
}

export default HamburgerMenuIcon
