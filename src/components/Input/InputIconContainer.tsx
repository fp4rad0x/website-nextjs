import styled from 'styled-components'
import InputIconContainerProps from '../../contracts/InputIconContainerProps'

const Input = styled.div<InputIconContainerProps>`
  display: flex;
  align-items: center;

  padding: 8px 0px 8px 15px;
  height: 2.5rem;
  font-size: 1.2rem;
  border-radius: ${(props) => (props.icon ? '4px 0px 0px 4px' : '4px')};
  margin: 0;
  border: none;
  background: ${(props) => props.theme.foreground.light};
  color: ${(props) => props.theme.background.dark};
  font-weight: 500;

  &:focus {
    outline: none;
  }
`

export default Input
