import React from 'react'
import InputComponent from './InputComponent'
import InputIconContainer from './InputIconContainer'
import Flex from '../Layout/Flex'
import InputProps from '../../contracts/InputProps'

const Input: React.FC<InputProps> = (props): JSX.Element => {
  const { icon } = props

  return (
    <Flex justifyContent="center" alignItems="center">
      {icon && <InputIconContainer icon={icon}>{icon}</InputIconContainer>}

      <InputComponent {...(props as React.ComponentPropsWithoutRef<'input'>)} />
    </Flex>
  )
}

export default Input
