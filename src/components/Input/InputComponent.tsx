import styled from 'styled-components'

const Input = styled.input`
  display: flex;
  align-items: center;

  padding: 8px 15px;
  height: 2.5rem;
  font-size: 1.2rem;
  border-radius: 0px 4px 4px 0px;
  margin: 0;
  border: none;
  background: ${(props) => props.theme.foreground.light};
  color: ${(props) => props.theme.background.dark};
  font-weight: 500;

  &:focus {
    outline: none;
  }
`

export default Input
