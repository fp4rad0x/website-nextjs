/* eslint-disable sonarjs/no-identical-expressions */
import styled from 'styled-components'
import ButtonProps from '../../contracts/ButtonProps'
import { darken } from 'polished'

const Button = styled.button<ButtonProps>`
  display: flex;
  align-items: center;
  gap: ${(props) => props.gap && props.gap};
  justify-content: ${(props) =>
    props.justifyContent ? props.justifyContent : 'center'};
  font-size: ${(props) => props.theme.fontSize.h4};
  font-weight: bold;
  width: ${(props) => props.fullWidth && '100%'};
  margin: ${(props) => props.margin && props.margin};
  margin-top: ${(props) => props.marginTop && props.marginTop};
  margin-bottom: ${(props) => props.marginBottom && props.marginBottom};
  margin-left: ${(props) => props.marginLeft && props.marginLeft};
  margin-right: ${(props) => props.marginRight && props.marginRight};
  height: 40px;
  padding: ${(props) => (props.padding ? props.padding : '5px 10px')};
  border-radius: 4px;
  color: ${(props) => props.theme.foreground.light};
  background-color: ${(props) => props.theme.highlight.logo.light};
  cursor: unset;
  transition: 200ms;

  /* background: linear-gradient(
    90deg,
    rgba(0, 141, 177, 1) 0%,
    rgba(0, 175, 177, 1) 11%,
    rgba(0, 178, 168, 1) 24%,
    rgba(0, 185, 151, 1) 36%,
    rgba(0, 190, 140, 1) 56%,
    rgba(0, 197, 125, 1) 78%,
    rgba(0, 212, 255, 1) 100%
  ); */

  &:hover {
    cursor: pointer;
    transition: 200ms;
    background-color: ${(props) =>
      darken(0.05, props.theme.highlight.logo.light)};
  }

  &:active {
    transition: 200ms;
    background-color: ${(props) =>
      darken(0.135, props.theme.highlight.logo.light)};
  }
`

export default Button
