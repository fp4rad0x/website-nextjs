import ANDROID_VERSION from '../models/Enums/ANDROID_VERSION'
import BUILD_TYPE from '../models/Enums/BUILD_TYPE'

const endpoints = {
  devices:
    'https://raw.githubusercontent.com/AOSPK/official_devices/master/devices.json',

  maintainers:
    'https://raw.githubusercontent.com/AOSPK/official_devices/master/team/maintainers.json',

  // TODO - generate types for all the params
  device: (
    androidVersion: ANDROID_VERSION,
    buildType: BUILD_TYPE,
    codename: string
  ) =>
    `https://raw.githubusercontent.com/AOSPK/official_devices/master/builds/${androidVersion}/${buildType}/${codename}.json`,

  specs: (codename: string) =>
    `https://raw.githubusercontent.com/AOSPK/official_devices/master/specs/${codename}.json`,

  image: (codename: string) =>
    `https://raw.githubusercontent.com/AOSPK/official_devices/master/images/devices/${codename}.png`,
}

export default endpoints
