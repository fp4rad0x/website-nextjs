import axios, { AxiosResponse } from 'axios'
import endpoints from './endpoints'

const fetchDeviceImage = async (
  codename: string
): Promise<AxiosResponse<ImageData>> => {
  try {
    return await axios.get<ImageData>(endpoints.image(codename))
  } catch (err) {
    console.error(err)
    return err
  }
}

export default fetchDeviceImage
