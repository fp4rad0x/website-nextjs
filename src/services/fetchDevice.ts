import axios, { AxiosResponse } from 'axios'
import BUILD_TYPE from '../models/Enums/BUILD_TYPE'
import endpoints from './endpoints'
import ANDROID_VERSION from '../models/Enums/ANDROID_VERSION'
import DeviceBuild from '../models/DeviceBuild'

const fetchDevice = async (
  androidVersion: ANDROID_VERSION,
  buildType: BUILD_TYPE,
  codename: string
): Promise<AxiosResponse<DeviceBuild>> => {
  try {
    return await axios.get<DeviceBuild>(
      endpoints.device(androidVersion, buildType, codename)
    )
  } catch (err) {
    console.error(err)
    return err
  }
}

export default fetchDevice
