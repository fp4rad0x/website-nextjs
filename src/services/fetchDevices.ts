import axios, { AxiosResponse } from 'axios'
import Device from '../models/Device'
import endpoints from './endpoints'

const fetchDevices = async (): Promise<AxiosResponse<Device[]>> => {
  try {
    return await axios.get<Device[]>(endpoints.devices)
  } catch (err) {
    console.error(err)
    return err
  }
}

export default fetchDevices
