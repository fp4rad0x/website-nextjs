import axios, { AxiosResponse } from 'axios'
import Maintainer from '../models/Maintainer'
import endpoints from './endpoints'

const fetchMaintainers = async (): Promise<AxiosResponse<Maintainer[]>> => {
  try {
    return await axios.get<Maintainer[]>(endpoints.maintainers)
  } catch (err) {
    console.error(err)
    return err
  }
}

export default fetchMaintainers
