import axios, { AxiosResponse } from 'axios'
import endpoints from './endpoints'
import DeviceSpecs from '../models/DeviceSpecs'

const fetchDeviceSpecs = async (
  codename: string
): Promise<AxiosResponse<Partial<DeviceSpecs>>> => {
  try {
    return await axios.get<Partial<DeviceSpecs>>(endpoints.specs(codename))
  } catch (err) {
    console.error(err)
    return err
  }
}

export default fetchDeviceSpecs
