import { useState, useEffect } from 'react'

/**
 * @param value {number} - The value to return
 */
const useDebounce = (value: unknown, time: number): typeof value => {
  const [debouncedValue, setDebouncedValue] = useState(value)

  useEffect(() => {
    const timeout = setTimeout(() => {
      setDebouncedValue(value)
    }, time)

    return () => {
      clearTimeout(timeout)
    }
  }, [value])

  return debouncedValue
}

export default useDebounce
