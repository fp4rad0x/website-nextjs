/* eslint-disable no-debugger */
import { useEffect, useState } from 'react'
import { MOBILE_WIDTH } from '../constants/constants'

const useIsMobile = (width: number = MOBILE_WIDTH): boolean => {
  const [isMobile, setIsMobile] = useState(false)

  useEffect(() => {
    if (window.innerWidth < width) {
      setIsMobile(true)
    } else {
      setIsMobile(false)
    }

    const handleResize = (): void => {
      if (window.innerWidth < width) {
        setIsMobile(true)
      } else {
        setIsMobile(false)
      }
    }

    window.addEventListener('resize', handleResize)

    return () => {
      window.removeEventListener('resize', handleResize)
    }
  }, [])

  return isMobile
}

export default useIsMobile
