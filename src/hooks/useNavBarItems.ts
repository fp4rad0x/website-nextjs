import { useTranslation } from 'react-i18next'
import LINKS from '../constants/links'
import ROUTES from '../constants/routes'
import { NavBarItem } from '../contracts/NavBarItem'

const useNavBarItems = (): NavBarItem[] => {
  const { t } = useTranslation()

  return [
    {
      label: t('navbar.features'),
      link: ROUTES.FEATURES,
    },
    {
      label: t('navbar.devices'),
      link: ROUTES.DEVICES,
    },
    {
      label: 'Gerrit',
      link: LINKS.GERRIT,
    },
  ]
}

export default useNavBarItems
