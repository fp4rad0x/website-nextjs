import React from 'react'

interface BrandFilterProps {
  label?: string
  active?: boolean
  onClick?: (evt: React.MouseEvent<HTMLDivElement>) => void
}

export default BrandFilterProps
