type FooterItem = {
  label: string
  linkTo: string
}

interface FooterItemProps {
  title: string
  items: FooterItem[]
}

export default FooterItemProps
