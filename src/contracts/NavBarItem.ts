export type NavBarItem = {
  label: string
  link: string
}
