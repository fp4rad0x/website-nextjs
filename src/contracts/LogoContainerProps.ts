import { Property } from 'csstype'

interface LogoContainerProps {
  cursor?: Property.Cursor
}

export default LogoContainerProps
