interface BuildCardProps {
  buildType: string
  size: string
  md5: string
  version: string
  filename: string
  url: string
}

export default BuildCardProps
