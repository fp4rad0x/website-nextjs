interface CardProps {
  title: string
  text: string
  img: StaticImageData
}

export default CardProps
