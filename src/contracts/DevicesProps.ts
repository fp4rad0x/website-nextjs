import Device from '../models/Device'
import Maintainer from '../models/Maintainer'

interface DevicesProps {
  devices: Device[]
  maintainers: Maintainer[]
  children?: React.ReactNode
}

export default DevicesProps
