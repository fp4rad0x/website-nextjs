interface FooterIconProps {
  onClick?: () => void
  children?: React.ReactNode
  toolTip?: string
}

export default FooterIconProps
