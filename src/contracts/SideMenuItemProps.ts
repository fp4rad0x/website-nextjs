import React from 'react'

interface SideMenuItemProps {
  icon?: React.ReactNode
}

export default SideMenuItemProps
