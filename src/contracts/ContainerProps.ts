import { Property } from 'csstype'

interface ContainerProps {
  backgroundColor?: Property.BackgroundColor

  height?: Property.Height
  minHeight?: Property.MinHeight
  maxHeight?: Property.MaxHeight

  width?: Property.Width
  minWidth?: Property.MinWidth

  gap?: Property.Gap
  justifyContent?: Property.JustifyContent
  flex?: boolean
  flexDirection?: Property.FlexDirection
  maxWidth?: Property.MaxWidth
  alignItems?: Property.AlignItems
  padding?: Property.Padding

  paddingTop?: Property.PaddingTop
  paddingBottom?: Property.PaddingBottom
  paddingRight?: Property.PaddingRight
  paddingLeft?: Property.PaddingLeft
}

export default ContainerProps
