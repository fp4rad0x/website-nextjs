import { Property } from 'csstype'

interface SideMenuItemContainerProps {
  color?: Property.Color
}

export default SideMenuItemContainerProps
