import { Property } from 'csstype'

export enum VARIANT {
  h1 = 'h1',
  h2 = 'h2',
  h3 = 'h3',
  h4 = 'h4',
  h5 = 'h5',
  h6 = 'h6',
  p = 'p',
}

export interface TextProps {
  fontWeight?: Property.FontWeight
  cursor?: Property.Cursor
  opacity?: Property.Opacity
  variant?: keyof typeof VARIANT
  bold?: boolean
  fontSize?: Property.FontSize
  color?: Property.Color | 'darken'
}
