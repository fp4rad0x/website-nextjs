import { Property } from 'csstype'

export enum NAVBAR_LOGO_TYPE {
  NAVBAR = 'NavBar',
  FULL = 'fullWidth',
}

interface LogoProps {
  text?: boolean
  type?: NAVBAR_LOGO_TYPE
  divider?: boolean
  cursor?: Property.Cursor
  onClick?: (evt: React.MouseEvent<HTMLDivElement>) => void
}

export default LogoProps
