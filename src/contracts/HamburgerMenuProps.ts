import React from 'react'

interface HamburgerMenuProps {
  isActive: boolean
  setIsActive: React.Dispatch<React.SetStateAction<boolean>>
}

export default HamburgerMenuProps
