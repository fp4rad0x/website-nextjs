import Device from '../models/Device'
import DeviceBuild from '../models/DeviceBuild'
import DeviceSpecs from '../models/DeviceSpecs'

interface DevicesProps extends DeviceBuild {
  deviceDetails?: Device | undefined
  deviceSpecs?: Partial<DeviceSpecs>
}

export default DevicesProps
