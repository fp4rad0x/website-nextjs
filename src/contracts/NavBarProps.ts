import React from 'react'
import { NavBarItem } from './NavBarItem'

interface NavBarProps {
  NavBarItems: NavBarItem[]
  menuActive: boolean
  setMenuActive: React.Dispatch<React.SetStateAction<boolean>>
}

export default NavBarProps
