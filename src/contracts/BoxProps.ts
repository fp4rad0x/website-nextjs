import { Property } from 'csstype'

interface BoxProps {
  height?: Property.Height
  width?: Property.Width
  maxWidth?: Property.MaxWidth

  top?: Property.Top
  bottom?: Property.Bottom
  left?: Property.Left
  right?: Property.Right

  background?: Property.Background
  backgroundColor?: Property.BackgroundColor

  padding?: Property.Padding
  paddingTop?: Property.PaddingTop
  paddingBottom?: Property.PaddingBottom
  paddingRight?: Property.PaddingRight
  paddingLeft?: Property.PaddingLeft

  margin?: Property.Margin
  marginTop?: Property.MarginTop
  marginBottom?: Property.MarginBottom
  marginRight?: Property.MarginRight
  marginLeft?: Property.MarginLeft

  textAlign?: Property.TextAlign

  position?: Property.Position

  cursor?: Property.Cursor
}

export default BoxProps
