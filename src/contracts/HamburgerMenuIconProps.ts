interface HamburgerMenuIconProps {
  menuActive: boolean
  onClick: () => void
}

export default HamburgerMenuIconProps
