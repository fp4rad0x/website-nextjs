import { Property } from 'csstype'

interface ButtonProps {
  variant?: 'primary' | 'secondary' | 'tertiary'
  padding?: Property.Padding
  fullWidth?: boolean
  margin?: Property.Margin
  marginTop?: Property.MarginTop
  marginBottom?: Property.MarginBottom
  marginLeft?: Property.MarginLeft
  marginRight?: Property.MarginRight

  gap?: Property.Gap
  justifyContent?: Property.JustifyContent
}

export default ButtonProps
