interface DeviceCardProps {
  brand: string
  deviceName: string
  deviceCodeName: string
  maintainer?: string
  deprecated: boolean
  xda_thread?: string
  supported_versions?: string
  supported_types?: string
  repositories?: string[]
}

export default DeviceCardProps
