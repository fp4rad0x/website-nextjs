import { Property } from 'csstype'

interface TagProps {
  color?: Property.Color
  variant: 'warning' | 'danger' | 'info' | 'ok'
  text?: string
  toolTip?: string
}

export default TagProps
