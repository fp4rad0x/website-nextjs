import { Property } from 'csstype'
interface PaperProps {
  backgroundColor?: Property.BackgroundColor
  borderRadius?: Property.BorderRadius

  height?: Property.Height
  width?: Property.Width

  minHeight?: Property.MinHeight
  minWidth?: Property.MinWidth

  maxHeight?: Property.MaxHeight
  maxWidth?: Property.MaxWidth

  justifyContent?: Property.JustifyContent
  direction?: Property.FlexDirection
  alignItems?: Property.AlignItems
  alignContent?: Property.AlignContent
  gap?: Property.Gap
  flexWrap?: Property.FlexWrap

  padding?: Property.Padding
  paddingTop?: Property.PaddingTop
  paddingBottom?: Property.PaddingBottom
  paddingRight?: Property.PaddingRight
  paddingLeft?: Property.PaddingLeft

  margin?: Property.Margin
  marginTop?: Property.MarginTop
  marginBottom?: Property.MarginBottom
  marginRight?: Property.MarginRight
  marginLeft?: Property.MarginLeft

  textAlign?: Property.TextAlign

  cursor?: Property.Cursor
}

export default PaperProps
