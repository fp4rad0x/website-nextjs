import { Property } from 'csstype'

interface HamburgerMenuContainerProps {
  left: Property.Left
}

export default HamburgerMenuContainerProps
