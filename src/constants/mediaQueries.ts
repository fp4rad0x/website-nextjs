import { MOBILE_WIDTH } from './constants'

const media = {
  desktop: `(max-width: ${MOBILE_WIDTH}px)`,
}

export default media
