enum ROUTES {
  HOME = '/',
  DEVICES = '/devices',
  FEATURES = '/features',
  DOWNLOAD = '/download',
}

export default ROUTES
