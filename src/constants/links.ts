enum LINKS {
  TELEGRAM = 'https://t.me/AOSPKNews',
  GITHUB = 'http://github.com/AOSPK',
  DONATION = 'https://bit.ly/mamutal91_buymeacoffe',
  PAYPAL = 'https://bit.ly/mamutal91_paypal',
  GERRIT = 'http://gerrit.aospk.org',
}

export default LINKS
