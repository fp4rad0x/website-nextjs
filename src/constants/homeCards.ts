interface Cards {
  title: string
  text: string
  img: StaticImageData
}

interface HomeCards {
  image1: StaticImageData
  image2: StaticImageData
  image3: StaticImageData
}

const HomeCards = ({ image1, image2, image3 }: HomeCards): Cards[] => [
  {
    title: 'Lorem Ipsum',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    img: image1,
  },
  {
    title: 'Lorem Ipsum',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    img: image2,
  },
  {
    title: 'Lorem Ipsum',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    img: image3,
  },
]

export default HomeCards
