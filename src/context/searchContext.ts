import React, { createContext } from 'react'

/* eslint-disable @typescript-eslint/no-explicit-any */
const SearchContext = createContext<{
  searchContext: string
  setSearchContext: React.Dispatch<React.SetStateAction<string>>
}>(null as any)

export default SearchContext
