import React, { createContext } from 'react'
import Maintainer from '../models/Maintainer'

/* eslint-disable @typescript-eslint/no-explicit-any */
const MaintainerContext = createContext<{
  maintainerContext: Maintainer[]
  setMaintainerContext: React.Dispatch<React.SetStateAction<Maintainer[]>>
}>(null as any)

export default MaintainerContext
