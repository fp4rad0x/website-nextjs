import React, { createContext } from 'react'
import Device from '../models/Device'

/* eslint-disable @typescript-eslint/no-explicit-any */
const DeviceContext = createContext<{
  deviceContext: Device[]
  setDeviceContext: React.Dispatch<React.SetStateAction<Device[]>>
}>(null as any)

export default DeviceContext
