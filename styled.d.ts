import 'styled-components'

declare module 'styled-components' {
  export interface Theme {
    theme: 'dark' | 'light'
    background: {
      dark: string
      light: string
      lighter: string
      paper: string
    }
    foreground: {
      dark: string
      light: string
    }
    highlight: {
      logo: {
        light: string
        dark: string
        blue: string
        highlight1: string
        highlight2: string
        highlight3: string
        highlight4: string
        highlight5: string
        highlight6: string
        highlight7: string
      }
      normal: string
      alert: string
      danger: string
    }
    // get ()
    // components: {
    //   button: {
    //     background: string | function
    //   }
    // }
    fontSize: {
      h1: string
      h2: string
      h3: string
      h4: string
      h5: string
      h6: string
      p: string
    }
  }
}
