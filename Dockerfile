FROM node:16

# RUN apt-get -y install nginx yarnpkg

WORKDIR /var/www/kraken-website

COPY ./package.json /var/www/kraken-website/

RUN yarn

COPY . /var/www/kraken-website/

RUN yarn build

EXPOSE 3000

CMD [ "yarn", "start" ]

# RUN ufw enable
# RUN ufw allow 'Ngnix Full'
